terraform {
  required_version = ">= 0.15"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.62"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 3.62"
    }
    random = {
      source = "hashicorp/random"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.2"
    }
    vault = {
      source  = "hashicorp/vault"
      version = ">= 2.19"
    }
    pagerduty = {
      source  = "pagerduty/pagerduty"
      version = ">= 2.0.0"
    }
  }
}

data "google_client_config" "default" {}

data "google_container_cluster" "cluster" {
  project  = "ffnproject-e7a5227126e9"
  name     = "stg"
  location = "us-central1"
}

provider "kubernetes" {
  host  = "https://${data.google_container_cluster.cluster.endpoint}"
  token = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(
    data.google_container_cluster.cluster.master_auth[0].cluster_ca_certificate,
  )
}

data "vault_generic_secret" "pagerduty" {
  path = "secret/internal/terraform/pagerduty"
}

provider "pagerduty" {
  token = data.vault_generic_secret.pagerduty.data["token"]
}
