module "svc_cplus_payment" {
  source  = "app.terraform.io/ffn/service/google"
  version = "~> 0.6.14"

  context = module.base.contexts.default

  svc_name    = "CPlus Payment"
  svc_key     = "cplus-payment"
  description = "Handles Cplus Disbursements"

  endpoints_enabled = true
  cloudsql_enabled  = true
  additional_project_roles = [
    "roles/pubsub.subscriber",
    "roles/pubsub.publisher",
    "roles/cloudtrace.agent",
  ]

  pagerduty_escalation_policy_id = "P17MGHX"
}

module "cplus_payment_mysql" {
  source  = "app.terraform.io/ffn/cloudsql/google"
  version = "~> 0.4.5"

  context = module.base.contexts.default

  name             = "${module.base.outputs.app_key}-mysql"
  database_version = "MYSQL_5_7"
  machine_type     = "db-n1-standard-1"
  disk_type        = "PD_SSD"
  maintenance_window = {
    day          = 6
    hour         = 10
    update_track = "stable"
  }

  database_flags = {
    sql_mode                        = "TRADITIONAL"
    log_bin_trust_function_creators = "on"
  }
  require_ssl   = true
  override_name = "po-payment-56475"
}

module "cplus_payment_db" {
  source  = "app.terraform.io/ffn/cloudsql/google//modules/database"
  version = "~> 0.5.2"

  context = module.cplus_payment_mysql.context
  name    = "po_payment"
}

#not needed, can be deleted in future
module "cplus_payment_qa_user" {
  source  = "app.terraform.io/ffn/cloudsql/google//modules/user"
  version = "~> 0.3.5"

  context = module.cplus_payment_mysql.context
  name    = "qa_user"
}

module "cplus_payment_app_user" {
  source  = "app.terraform.io/ffn/cloudsql/google//modules/user"
  version = "~> 0.3.5"

  context = module.cplus_payment_mysql.context
  name    = "app_user"
}
