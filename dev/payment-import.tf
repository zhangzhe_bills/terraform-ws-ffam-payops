module "svc_payment_import" {
  source  = "app.terraform.io/ffn/service/google"
  version = "~> 0.7.5"

  context = module.base.contexts.default

  svc_name    = "Payment Import Batch"
  svc_key     = "payment-import"
  description = "Payment Import Batch"

  pagerduty_escalation_policy_id = "P17MGHX"
}
