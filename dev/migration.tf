resource "google_project_iam_member" "migration_terraform" {
  for_each = toset([
    "roles/iam.serviceAccountAdmin",
  ])

  project = module.base.outputs.project_id
  role    = each.value
  member  = "serviceAccount:${module.base.outputs.legacy_tf_service_account}"
}