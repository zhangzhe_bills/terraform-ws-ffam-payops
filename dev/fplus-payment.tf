module "svc_fplus_payment" {
  source  = "app.terraform.io/ffn/service/google"
  version = "~> 0.6.14"

  context = module.base.contexts.default

  endpoints_enabled = true

  svc_name    = "Fplus Payment"
  svc_key     = "fplus-payment"
  description = "Handles integration with PCL to disburse funds for Fplus Loans."

  pagerduty_escalation_policy_id = "P17MGHX"
}