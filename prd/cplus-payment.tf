module "svc_cplus_payment" {
  source  = "app.terraform.io/ffn/service/google"
  version = "~> 0.6.14"

  context = module.base.contexts.default

  svc_name    = "CPlus Payment"
  svc_key     = "cplus-payment"
  description = "Handles Cplus Disbursements"

  pagerduty_escalation_policy_id = "P17MGHX"
}