terraform {
  backend "remote" {
    organization = "ffn"
    workspaces {
      name = "ffam-payops-prd"
    }
  }
}

provider "google" {
  region = "us-central1"
}

provider "google-beta" {
  region = "us-central1"
}

module "base" {
  source  = "app.terraform.io/ffn/remote-state/terraform"
  version = "~> 0.2.8"

  workspace = {
    name      = "ffam-app-payops"
    child_key = "prd"
  }
  additional_workspaces = {
    business_unit = {
      name      = "ffam-prd"
      child_key = "default"
    }
  }
}
